import INTP from './Icons/INTP.png';
import INTJ from './Icons/INTJ.png';
import ENTJ from './Icons/ENTJ.png';
import ENTP from './Icons/ENTP.png';
import INFJ from './Icons/INFJ.png';
import INFP from './Icons/INFP.png';
import ENFJ from './Icons/ENFJ.png';
import ENFP from './Icons/ENFP.png';
import ISTJ from './Icons/ISTJ.png';
import ISFJ from './Icons/ISFJ.png';
import ESTJ from './Icons/ESTJ.png';
import ESFJ from './Icons/ESFJ.png';
import ISTP from './Icons/ISTP.png';
import ISFP from './Icons/ISFP.png';
import ESTP from './Icons/ESTP.png';
import ESFP from './Icons/ESFP.png';

export const PersonalityTypes = {
    INTJ: {
        name:'Architect',
        explanation: `An Architect (INTJ) is a person with the Introverted, Intuitive, Thinking, and Judging
                     personality traits. These thoughtful tacticians love perfecting the details of life, applying
                     creativity and rationality to everything they do. Their inner world is often a private,
                     complex one.`,
        img: INTJ
    },
    INTP: {
        name:'Logician',
        explanation: `The Logician personality type is fairly rare, making up only three percent of the population,
                     which is definitely a good thing for them, as there’s nothing they’d be more unhappy about than
                     being “common”.`,
        img: INTP
    },
    ENTJ: {
        name:'Commander',
        explanation: `A Commander (ENTJ) is someone with the Extraverted, Intuitive, Thinking, and Judging
                     personality traits. They are decisive people who love momentum and accomplishment. They gather
                     information to construct their creative visions but rarely hesitate for long before acting on
                     them.`,
        img: ENTJ
    },
    ENTP: {
        name:'Debater',
        explanation: `A Debater (ENTP) is a person with the Extraverted, Intuitive, Thinking, and Prospecting
                     personality traits. They tend to be bold and creative, deconstructing and rebuilding ideas
                     with great mental agility. They pursue their goals vigorously despite any resistance they
                     might encounter.`,
        img: ENTP
    },
    INFJ: {
        name:'Advocate',
        explanation: `An Advocate (INFJ) is someone with the Introverted, Intuitive, Feeling, and Judging
                     personality traits. They tend to approach life with deep thoughtfulness and imagination.
                     Their inner vision, personal values, and a quiet, principled version of humanism guide them
                     in all things.`,
        img: INFJ
    },
    INFP: {
        name:'Mediator',
        explanation: `A Mediator (INFP) is someone who possesses the Introverted, Intuitive, Feeling, and
                     Prospecting personality traits. Making up only 4% of the population, these rare personality
                     types tend to be quiet, open-minded, imaginative, and apply a caring and creative approach to
                     everything they do.`,
        img: INFP
    },
    ENFJ: {
        name:'Protagonist',
        explanation: `A Protagonist (ENFJ) is a person with the Extraverted, Intuitive, Feeling, and Judging
                     personality traits. These warm, forthright types love helping others, and they tend to have
                     strong ideas and values. They back their perspective with the creative energy to achieve their
                     goals.`,
        img: ENFJ
    },
    ENFP: {
        name:'Campaigner',
        explanation: `A Campaigner (ENFP) is someone with the Extraverted, Intuitive, Feeling, and Prospecting
                     personality traits. These people tend to embrace big ideas and actions that reflect their
                     sense of hope and goodwill toward others. Their vibrant energy can flow in many directions.`,
        img: ENFP
    },
    ISTJ: {
        name:'Logistician',
        explanation: `A Logistician (ISTJ) is someone with the Introverted, Observant, Thinking, and Judging
                     personality traits. These people tend to be reserved yet willful, with a rational outlook
                     on life. They compose their actions carefully and carry them out with methodical purpose.`,
        img: ISTJ
    },
    ISFJ: {
        name:'Defender',
        explanation: `A Defender (ISFJ) is someone with the Introverted, Observant, Feeling, and Judging personality
                     traits. These people tend to be warm and unassuming in their own steady way. They’re efficient
                     and responsible, giving careful attention to practical details in their daily lives.`,
        img: ISFJ
    },
    ESTJ: {
        name:'Executive',
        explanation: `An Executive (ESTJ) is someone with the Extraverted, Observant, Thinking, and Judging
                     personality traits. They possess great fortitude, emphatically following their own sensible
                     judgment. They often serve as a stabilizing force among others, able to offer solid direction
                     amid adversity.`,
        img: ESTJ
    },
    ESFJ: {
        name:'Consul',
        explanation: `A Consul (ESFJ) is a person with the Extraverted, Observant, Feeling, and Judging personality
                     traits. They are attentive and people-focused, and they enjoy taking part in their social
                     community. Their achievements are guided by decisive values, and they willingly offer guidance
                     to others.`,
        img: ESFJ
    },
    ISTP: {
        name:'Virtuoso',
        explanation: `A Virtuoso (ISTP) is someone with the Introverted, Observant, Thinking, and Prospecting
                     personality traits. They tend to have an individualistic mindset, pursuing goals without
                     needing much external connection. They engage in life with inquisitiveness and personal skill,
                     varying their approach as needed.`,
        img: ISTP
    },
    ISFP: {
        name: 'Adventurer',
        explanation: `An Adventurer (ISFP) is a person with the Introverted, Observant, Feeling, and Prospecting
                     personality traits. They tend to have open minds, approaching life, new experiences, and
                     people with grounded warmth. Their ability to stay in the moment helps them uncover exciting
                     potentials.`,
        img: ISFP
    },
    ESTP: {
        name: 'Entrepreneur',
        explanation: `An Entrepreneur (ESTP) is someone with the Extraverted, Observant, Thinking, and Prospecting
                     personality traits. They tend to be energetic and action-oriented, deftly navigating whatever
                     is in front of them. They love uncovering life’s opportunities, whether socializing with others
                     or in more personal pursuits.`,
        img: ESTP
    },
    ESFP: {
        name: 'Entertainer',
        explanation: `An Entertainer (ESFP) is a person with the Extraverted, Observant, Feeling, and Prospecting
                     personality traits. These people love vibrant experiences, engaging in life eagerly and taking
                     pleasure in discovering the unknown. They can be very social, often encouraging others into
                     shared activities.`,
        img: ESFP
    },
}