import React from 'react';
import { Typography, Paper } from '@material-ui/core';
import { useStyles } from './PersonalityPageStyle';
import { PersonalityTypes } from '../../Resources/personalityTypes'


export const PersonalityPage = (props) => {
    const classes = useStyles();
    const personality = PersonalityTypes[props.match.params.type];
    return (
        <div className={classes.root}>
            <div className={classes.title} >The personality type is {personality.name}</div>
            <div className={classes.explanationBlock}>
                <Paper className={classes.paper}>{personality.explanation}</Paper>
                <img src={personality.img}></img>
            </div>
        </div>
    );
}

