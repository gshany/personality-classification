import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        display: "inline-flex"
    },
    title: {
        fontSize: "25pt",
        fontWeight: "bold",
        marginTop: "1%"
    },
    explanationBlock: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    paper: {
        width: "44%",
        height: "50%",
        fontSize: "20pt",
        textAlign: "left",
        borderRadius: "5%",
        padding: "1%",
        color:"#eaeaea",
        backgroundColor: "RGB(29,161,242)"
    },
})