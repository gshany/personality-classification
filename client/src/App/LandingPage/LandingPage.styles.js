export const styles = {
    paper : {
        width: "44%",
        height: "50%",
        margin: "auto",
        marginTop: "8%",
        borderRadius: "5%"
    },
    paperText : {
        paddingTop: "12%",
        userSelect: "none"
    },
    textField:{
        opacity: 0.7,
        backgroundColor: "#eaeaea"
    },
    sendButton: {
        marginTop:"3%",
        backgroundColor: "RGB(29,161,242)"
    },
    loader: {
        border: "16px solid #f3f3f3", 
        borderTop: "16px solid #3498db",
        borderRadius: "50%",
        width: "120px",
        height: "120px",
        animation: "$spin 2s linear infinite",
        margin: "auto",
        marginTop: "31vh"
      },
      '@keyframes spin': {
        "0%": { transform: 'rotate(0deg)' },
        "100%": { transform: 'rotate(360deg)' }
      },
      loaderTitle: {
          marginTop: '6vh',
          color: 'gray',
          fontSize: '1.3em',
          fontFamily: 'cursive',
          animation: "$fade 2s linear infinite",
          opacity: 1
      },
      '@keyframes fade': {
         "0%,100%": { opacity: 0 },
         "50%": { opacity: 1 }
      },
}