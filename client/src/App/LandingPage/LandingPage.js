import React from 'react';
import { useState, useEffect } from 'react';
import { Typography, TextField, Button, Paper, withStyles } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { styles } from './LandingPage.styles';
import Swal from 'sweetalert2'

const LandingPage = (props) => {
    const NO_PERSONALITY_TYPE_FOUND = 'No Statuses Found';
    let personalityType = null;

    const [searchedPersonName, setSearchedPersonName] = useState('');
    const [didSearchPersonalityType, setdidSearchPersonalityType] = useState(false);

    const searchPersonalityType = () => {
        // Set a loading bar
        setdidSearchPersonalityType(true);

        fetch(`http://localhost:8080/recognize?name=${searchedPersonName}`, {
            method: 'GET'
        }).then(async (response) => {
            personalityType = await response.text();

            if (personalityType === NO_PERSONALITY_TYPE_FOUND) {
                return Promise.reject(['no personality found :(', 'please try a different name']);
            } else {
                // Redirect to the given personality type
                props.history.push(`/personality/${personalityType}`);
            }
        }, () => Promise.reject(['Internal Server Error', 'Server might be down ):']))
        // Catch Any error and remove searching bar
        .then(undefined, (err) => Swal.fire(...err) && setdidSearchPersonalityType(false))
    }

    useEffect(() => {
        const onEnterClick = (e) => e.keyCode === 13 && searchPersonalityType();

        document.addEventListener('keydown', onEnterClick);

        return () => document.removeEventListener('keydown', onEnterClick)
    });

    const { classes } = props;
    return (
        (didSearchPersonalityType && !personalityType) ?
            <div>
                <div className={classes.loader} />
                <div className={classes.loaderTitle}>Calculating personality...</div>
            </div>
            : (
                <Paper elevation={3} className={classes.paper}>
                    <Typography variant="h5" className={classes.paperText}>Enter Twitter account name</Typography>
                    <div style={{ marginTop: "6%" }}>
                        <TextField id="standard-basic" className={classes.textField} placeholder={"@"}
                            onChange={e => setSearchedPersonName(e.target.value)} />
                        <br />
                        <Button
                            className={classes.sendButton}
                            variant="contained"
                            color="primary"
                            endIcon={<SearchIcon />}
                            onClick={searchPersonalityType}
                        >
                            Find personality type
                </Button>
                    </div>
                </Paper>
            )
    );
}

export default withStyles(styles)(LandingPage);
