import React from 'react';
import AppBar from './AppBar/AppBar';
import LandingPage from './LandingPage/LandingPage';
import './App.css';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import { PersonalityPage } from './PersonalityPage/PersonalityPage';

function App() {
  return (
    <div className="App"> 
      <Router>
      <AppBar label="What is your personality type"></AppBar>
        <Switch>
          <Route exact path='/' component={LandingPage} />
          <Route path='/personality/:type' component={PersonalityPage}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
