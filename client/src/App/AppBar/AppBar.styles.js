export const styles = {
    headerDiv: {
        height: "10%",
        backgroundColor: "RGB(29,161,242)"
    },
    backBtn: {
        height: "70%",
        display: "inline-block",
        position: "relative",
        top: "15%",
        float: "right",
        opacity: "0.5",
        transform: "rotate(180deg)",
        cursor: "pointer"
    },
    headerText: {
        color: "#ffffff",
        width: "fit-content",
        position: "relative",
        fontFamily: "arial",
        fontWeight: "bold",
        fontSize: "x-large",
        display: "inline-block",
        float: "left",
        paddingLeft: "1%",
        userSelect: "none"
    }
}