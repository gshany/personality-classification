import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import { styles } from './AppBar.styles';
import { withRouter } from "react-router-dom";

class AppBar extends React.Component {
    render() {
        const {classes} = this.props;
        return ( 
            <div className={classes.headerDiv}>
                <h1 className={classes.headerText}>{this.props.label}</h1>
                <img onClick={this.goBigOrGoHome} src={require('../../Resources/Icons/backBtn.png')} className={classes.backBtn} />
            </div>
            )
    }

    goBigOrGoHome = () => {
        this.props.history.push('/');
    }
}

export default withRouter(withStyles(styles)(AppBar));
