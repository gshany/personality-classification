import requests
from bs4 import BeautifulSoup

URL = "https://twitter.com/hashtag/16Personalities?src=hashtag_click"

# sending get request and saving the response as response object 
page = requests.get(url = URL) 

# Create a BeautifulSoup object
soup = BeautifulSoup(page.text, 'html.parser')
print(soup.text)
print(soup.findAll('article'))