<div align="center">
<div><img width=120 src="/uploads/286a9b69a731287277ef22eb5063269b/image.png"/></div>

# Personality Classification

[![Gem Version](http://img.shields.io/gem/v/badgerbadgerbadger.svg?style=flat-square)](https://rubygems.org/gems/badgerbadgerbadger) [![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org) [![Badges](http://img.shields.io/:badges-3/3-ff6799.svg?style=flat-square)](https://github.com/badges/badgerbadgerbadger)
</div>  


This project is a personality classifier that was trained on thousands of
real life examples, and can predict one of the sixteen accepted personality types
of any given person.  

## Project Components
- Python server side
- React client side
- Fined tuned Bert Neural network model
- Corpus data of 8600 tested examples

<div align="center">
<img src="/uploads/41ae96c5db7fc208dab1431d3cb17648/image.png" width=700 />
</div>

