
import csv
import time
from twitterscraper import query_tweets_from_user

USER_TO_CLASSIFICATION_FILE_PATH = 'C:/Git/personality-classification/src/twitter_scraper/twitter.csv'
USER_TWEETS_FILE = 'user_tweets.csv'
WAIT_TIME_ON_FAILURE = 2

def get_user_tweets(name):
    twitter_data = []
    for tweet in query_tweets_from_user(name, 50)[:50]:
        try:
            twitter_data.append(tweet.text)
        except:
            print("Could not add " + name + " data")

    return twitter_data


def write_user_tweets(p_type, text_arr, write_header=False):
    try:
        with open('user_tweets.csv', 'a', newline='') as csvfile:
            fieldnames = ['p_type', 'text']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            if write_header:
                writer.writeheader()

            for text in text_arr:
                try:
                    writer.writerow(
                        {'p_type': p_type, 'text': text})
                except:
                    pass
    except:
        print("Could not access file:" + USER_TWEETS_FILE + ", maybe already open?, will try agian in " + WAIT_TIME_ON_FAILURE + " Seconds")
        time.sleep(WAIT_TIME_ON_FAILURE)
        write_user_tweets(p_type, text_arr, write_header)


with open(USER_TO_CLASSIFICATION_FILE_PATH, newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    
    # Init user tweets file
    write_user_tweets('', [], True)
    
    for row in reader:
        user_name = row['user_name']
        p_type = row['p_type']
        user_tweets = get_user_tweets(user_name)
        write_user_tweets(p_type, user_tweets)