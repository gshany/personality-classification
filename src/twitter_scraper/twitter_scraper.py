import csv
from twitterscraper import query_tweets
twitter_data = []
personality_types = ["ISTJ", "ISFJ", "INFJ", "INTJ",
                     "ISTP", "ISFP", "INFP", "INTP", "ESTP", "ESFP",
                     "ENFP", "ENTP", "ESTJ", "ESFJ", "ENFJ", "ENTJ"]

for tweet in query_tweets("16Personalities", 5000):
    for p_type in personality_types:
        if p_type in tweet.text:  # filtering posts that doesn't contain personality type
            twitte = {}
            twitte['user_name'] = tweet.screen_name
            twitte['p_type'] = p_type
            twitte['text'] = tweet.text
            twitter_data.append(twitte)
            print("Name: " + tweet.screen_name + " Text: " + tweet.text)
            break


with open('twitter.csv', 'w', newline='') as csvfile:
    fieldnames = ['user_name', 'p_type', 'text']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for twitte in twitter_data:
        try:
            writer.writerow(twitte)
        except:
            pass

print("Done " + str(len(twitter_data)) + " twittes were found")
