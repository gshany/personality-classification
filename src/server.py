from flask import Flask
from flask_cors import CORS
from flask import request

# Imports from files
from twitter_requester import Twitter
from personalityRecognition import PersonalityRecognition

app = Flask(__name__)
CORS(app)
trainedModel = PersonalityRecognition()
twitter_api = Twitter()

@app.route("/")
def imAlive():
    return "Im Alive"

"""
Request example:
$.ajax({
  url: "http://localhost:8080/recognize",
  type: "get",
  data: { 
    name: "realDonaldTrump"
  },
  success: function(response) {
    console.log(response);
  }});

  More users:
  BarackObama
  katyperry
  taylorswift13
  ladygaga
  TheEllenShow
  ArianaGrande
  MileyCyrus
  BrunoMars
"""
@app.route("/recognize")
def recognizePersonality():
    # Fetch user tweets from twitter
    user_name = request.args.get('name')
    user_statuses = twitter_api.get_user_tweets(user_name, 100)
    
    # If no statuses were found in twitter
    if(len(user_statuses) == 0):
      return "No Statuses Found"

    # Map statuses to the status text
    get_text = lambda s: s.text
    user_text = list(map(get_text, user_statuses))

    # Recognize personality according to the text
    personality = trainedModel.recognize(user_text)
    return personality


if __name__ == "__main__":
    app.run(port=8080)