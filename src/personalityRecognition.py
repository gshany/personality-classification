import torch
from pytorch_pretrained_bert import BertForSequenceClassification, BertTokenizer
from keras.preprocessing.sequence import pad_sequences
import numpy as np

class PersonalityRecognition:
    def __init__(self):
        self.label_map = ['INFP', 'ISFP', 'ENFJ', 'INTJ', 'ISFJ', 'INFJ', 'ENTJ', 'ESTP', 'ESTJ', 'ESFJ', 'INTP', 'ISTP', 'ENFP', 'ESFP', 'ISTJ', 'ENTP']
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-cased', do_lower_case=False)
        self.model = BertForSequenceClassification.from_pretrained("bert-base-cased", num_labels=16)
        self.model.load_state_dict(torch.load('./MBTIbert-twitter.pt', map_location=torch.device('cpu')))
        self.model.eval()
        # self.model.cuda()
        print('Finished loading model')

    def recognize(self, data: list):
        input_ids = torch.tensor(self.get_input_ids(data)).to(torch.int64)
        attention_mask = torch.tensor(self.get_attention_mask(input_ids))
        with torch.no_grad():
            # Forward pass, calculate logit predictions
            logits = self.model(input_ids, token_type_ids=None, attention_mask=attention_mask)
        logits = logits.detach().cpu().numpy()
        data_personality_types = list(np.argmax(logits, axis=1).flatten()) 
        max_personality_type = self.get_max_occurrences(data_personality_types)
        personality_type_str = self.label_map[max_personality_type]
        return personality_type_str 

    def get_max_occurrences(self, arr):
        return max(arr,key=arr.count)

    # Adding prefix and suffix to each sentence.
    def add_pre_and_suf_fixes(self, sentences):
        new_sentences = []
        for sentence in sentences:
            new_sentences.append("[CLS] " + sentence + " [SEP]")
        return new_sentences

    def get_attention_mask(self, input_ids):
        # Create attention masks
        attention_masks = []

        # Create a mask of 1s for each token followed by 0s for padding
        for seq in input_ids:
            seq_mask = [float(i>0) for i in seq]
            attention_masks.append(seq_mask)
        
        return attention_masks

    def get_input_ids(self, sentences):
        sentences = self.add_pre_and_suf_fixes(sentences)
        tokenized_texts = [self.tokenizer.tokenize(sent) for sent in sentences]

        # Use the BERT tokenizer to convert the tokens to their index numbers in the BERT vocabulary
        input_ids = [self.tokenizer.convert_tokens_to_ids(x) for x in tokenized_texts]

        # Pad our input tokens
        input_ids = pad_sequences(input_ids, maxlen=211, dtype="long", truncating="post", padding="post")

        return input_ids
